﻿$(document).ready(function () {

});
var EbipsApp = angular.module('EbipsApp', []);
function getNgScope() {
    var appElement = document.querySelector('[ng-app=EbipsApp]');
    return angular.element(appElement).scope();
};
var base = "http://localhost:54572";
EbipsApp.controller('EbipsRevCtrlr', function ($scope, $http) {
    $scope.processing = false;
    $scope.rowCollection = [{}];

    //build datatable
    $scope.loadDataTable = function () {
        var lstRows = $scope.rowCollection;
        var hr = '';
        $("#listOfRecord").html("");
        hr = '<table id="datatable" class="table table-striped table-bordered table-actions-bar">';
        hr += '<thead><tr>';
        hr += '<th>S/N </th>';
        hr += '<th>Engineer Id</th>';
        hr += '<th>Schedule Id</th>';
        hr += '<th>Shift</th>';
        hr += '<th>shift Date</th>';
        hr += '</tr></thead><tbody>';
        $.each(lstRows, function (i, rec) {

            hr +=
                "<tr><td>" + rec.SN +
            "</td><td>" + rec.engineerId +
            "</td><td>" + rec.scheduleId +
            "</td><td>" + rec.shift +
            "</td><td>" + rec.shiftDate + "</td>";
           
            hr += "</tr>";
        });
        hr += '</tbody>';

        hr += '</table>';
        $("#listOfRecord").html(hr);
    };
    $scope.loadDataTable();

    // call api to get  Schedules
    $scope.getSchedules = function () {
        $.ajax({
            type: "GET",
            url: base + "/api/Schedules/Get",
            async: false
        })
            .success(function (data) {
                console.log(data);
                var resultData = [];
                var sn = 1;
                $.each(data, function (i, rec) {
                    rec.SN = sn;
                    resultData.push(rec);
                    sn++;
                });

                $scope.rowCollection = resultData;
                $scope.loadDataTable();

            })
            .error(function (data) {

                $scope.rowCollection = {};
                $scope.loadDataTable();
            });
    };
    $scope.getSchedules();

    // call api to generate  Schedules
    $scope.generateSchedules = function () {
        $.ajax({
            type: "GET",
            url: base + "/api/Schedules/Generate",
            async: false
        })
            .success(function (data) {
                console.log(data);
                var resultData = [];
                var sn = 1;
                $.each(data, function (i, rec) {
                    rec.SN = sn;
                    resultData.push(rec);
                    sn++;
                });

                $scope.rowCollection = resultData;
                $scope.loadDataTable();

            })
            .error(function (data) {

                $scope.generateSchedules();
            });
    };


});

﻿using Axceligent.Core.Data.DB;
using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axceligent.Core.Data.Repository
{
    
    public class ShiftRepository : BaseRepository<ShiftEntity, int>
    {
        public ShiftRepository(AxDbContext context) : base(context)
        {
        }

        public IQueryable<ShiftEntity> Search(int engineerId = 0, int scheduleId = 0)
        {
            var table = Query();
            
            if (engineerId > 0)
            {
                table = table.Where(x => x.EngineerId == engineerId);
            }
            if (scheduleId > 0)
            {
                table = table.Where(x => x.ScheduleId == scheduleId);
            }

            return table;
        }

        public bool ItemExists(int engineerId = 0, int scheduleId = 0, int Id = 0)
        {
            var check = Search(engineerId, scheduleId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        public bool ItemExists(ShiftModel model, long Id = 0)
        {
            var check = Search(model.EngineerId, model.ScheduleId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Models
{
    public class ShiftModel: ModelBase<int>
    {
        public int EngineerId { get; set; }
        public int ScheduleId { get; set; }
        public string Shift
        {
            get
            {
                if (ScheduleId % 2 == 0)
                {
                    return "Afternoon Shift";
                }
                return "Morning Shift";
            }

        }

        public string ShiftDate
        {
            get
            {
                if (ScheduleId % 2 == 1)
                {
                    return DateTime.Now.StartOfWeek(DayOfWeek.Monday).Date.AddDays(ScheduleId / 2).ToShortDateString();
                }

                else
                {
                    return DateTime.Now.StartOfWeek(DayOfWeek.Monday).Date.AddDays((ScheduleId -1) / 2).ToShortDateString();
                }

            }
        }
    }
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (14 + (dt.Date.DayOfWeek - startOfWeek)) % 7;
            var a = dt.Date.AddDays(-1 * diff).Date;
            return a.AddDays(7).Date;
        }
    }

}

﻿using Axceligent.Core.Common;
using Axceligent.Core.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Data.DB
{
     
    public class AxDbContext : DbContext
    {
        public AxDbContext(DbContextOptions <AxDbContext> options)
            : base(options)
        {

        }
        public DbSet<EntitySetting> EntitySettings { get; set; }
        public DbSet<ShiftEntity> Shifts { get; set; }
    }
}

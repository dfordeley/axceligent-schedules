﻿using Axceligent.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Axceligent.Core.Domain.Entity
{
    public class EntityBase<T> : ReportingBase
    {

        /// <summary>
        /// 
        /// </summary>
        [Key]
        public T Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public RecordStatus RecordStatus { get; set; }
    }

}

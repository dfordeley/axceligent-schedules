﻿using Axceligent.Core.Domain.Factory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Module
{
    public class FactoryModule : IFactoryModule
    {

        private EntitySettingFactory _entitysetting;
        private EngineerFactory _engineer;
        private ShiftFactory _shift;
        /// <summary>
        /// EntitySetting Factory Module
        /// </summary>
        public EntitySettingFactory EntitySettings { get { if (_entitysetting == null) { _entitysetting = new EntitySettingFactory(); } return _entitysetting; } }

        public EngineerFactory Engineers { get { if (_engineer == null) { _engineer = new EngineerFactory(); } return _engineer; } }

        public ShiftFactory Shifts { get { if (_shift == null) { _shift = new ShiftFactory(); } return _shift; } }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Common
{
    public class Constants
    {
        public static string ShortDate = "dd-MMM-yyyy";
        public static string LongDate = "dd-MMM-yy hh:mm tt";
        public static string DBName
        {
            get
            {
                return Settings.Get("db.name", "DefaultConnection");
            }
        }

        public static string LogPath
        {
            get
            {
                return Settings.Get("log.path", "c:/temp/HT/");
            }
        }
    }
}

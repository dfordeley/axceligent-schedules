﻿using Axceligent.Core.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axceligent.Core.Domain.Helper
{
    public class EntitySettingHelper
    {
        private List<EntitySetting> _list;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        public EntitySettingHelper(List<EntitySetting> list)
        {
            _list = list;
        }
        /// <summary>
        /// Get string value of item in list
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValue(string key)
        {
            try
            {
                var k = FindKeyValue(key);
                if (k == null)
                {
                    return string.Empty;
                }
                return k.SVal;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Find a key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public EntitySetting FindKeyValue(string key)
        {
            return _list.Where(x => x.SKey == key).FirstOrDefault();
        }

        /// <summary>
        /// get Int Value of item in list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public int GetInt(string key, int otherwise = 0)
        {
            try
            {
                var k = GetValue(key.ToUpper());
                if (string.IsNullOrEmpty(k)) return otherwise;
                return Convert.ToInt32(k);
            }
            catch
            {
                return otherwise;
            }
        }

        /// <summary>
        /// get Long Value of item in list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public long GetLong(string key, long otherwise = 0)
        {
            try
            {
                var k = GetValue(key.ToUpper());
                if (string.IsNullOrEmpty(k)) return otherwise;
                return Convert.ToInt64(k);
            }
            catch
            {
                return otherwise;
            }
        }

        /// <summary>
        /// get boolean Value of item in list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public bool? GetBoolean(string key, bool otherwise = false)
        {
            try
            {
                var k = GetValue(key.ToUpper());
                if (string.IsNullOrEmpty(k)) return otherwise;
                return Convert.ToBoolean(GetValue(key));
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// get Datetime Value of item in list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public DateTime GetDateTime(string key, string otherwise = "")
        {
            if (string.IsNullOrEmpty(otherwise))
            {
                otherwise = new DateTime(1900, 1, 1).ToString();
            }

            var dtm = DateTime.Now;
            try
            {
                dtm = Convert.ToDateTime(otherwise);
                var k = GetValue(key);
                if (string.IsNullOrEmpty(k)) return dtm;
                return Convert.ToDateTime(k);
            }
            catch
            {
                return dtm;
            }
        }


        public List<EntitySetting> GetList()
        {
            return _list;
        }
    }
}

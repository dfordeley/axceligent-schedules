﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Enums
{
    /// <summary>
    /// Status of a record in the database
    /// </summary>
    public enum RecordStatus
    {
        /// <summary>
        /// Pendinf state
        /// </summary>
        Pending,
        /// <summary>
        /// Flagged as active
        /// </summary>
        Active,
        /// <summary>
        /// Flagged as inactive
        /// </summary>
        Inactive,
        /// <summary>
        /// Soft deleted
        /// </summary>
        Deleted,
        /// <summary>
        /// Archivere can delete and archive record
        /// </summary>
        Archive
    }

}

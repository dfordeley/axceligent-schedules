﻿using Axceligent.Core.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Entity
{
    public class ReportingBase
    {
        public ReportingBase()
        {
            CreatedAt = Util.CurrentDateTime();
            UpdatedAt = Util.CurrentDateTime();
        }
        /// <summary>
        /// Date Record was created
        /// </summary> 
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Last updated date
        /// </summary>
        public DateTime UpdatedAt { get; set; }
    }
}

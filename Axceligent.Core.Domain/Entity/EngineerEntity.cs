﻿using Axceligent.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Entity
{
    public class EngineerEntity: EntityBase<int>
    {
        public int ScheduleCount { get; set; }
        public int LastScheduleId { get; set; }
        public States LastState { get; set; }
        public int NumberOfHoliday { get; set; }
    }
}

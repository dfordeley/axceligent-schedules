﻿using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace Axceligent.Core.Common
{
    public static class Settings
    {

        public static string Connection()
        {

            //var str = ConfigurationManager.ConnectionStrings[Constants.DBName].ConnectionString;
            //return str;
            return Constants.DBName;
        }

        public static string ConnectionString(string key = "DefaultConnection")
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        public static string Get(string key, string otherwise = "")
        {
            var env = ConfigurationManager.AppSettings["site.env"];
            //if (env == "dev")
            //{
            //    try
            //    {
            //        var dir = new System.IO.DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory);
            //        var path = dir.Parent.FullName + "//app.devconfig";
            //        if (!string.IsNullOrEmpty(path))
            //        {
            //            if (System.IO.File.Exists(path))
            //            {
            //                var _settings = System.IO.File.ReadAllText(path);
            //                var dc = Util.DeserializeJSON<DevConfig>(_settings);
            //                var found = dc.Settings.Where(x => x.Key == key).FirstOrDefault();
            //                if (found != null)
            //                    return found.Value;
            //            }
            //        }
            //    }
            //    catch { }
            //}
            var str = ConfigurationManager.AppSettings[key];
            if (!string.IsNullOrEmpty(str)) return str;
            else return otherwise;
        }

        /// <summary>
        /// GEt a integer value from the apps settings
        /// </summary>
        /// <param name="key">The appsetting key</param>
        /// <param name="otherwise">Default value if nothing is found</param>
        /// <returns></returns>
        public static int GetInt(string key, int otherwise = 0)
        {
            var str = Get(key);
            var val = otherwise;
            try
            {
                val = int.Parse(str);
            }
            catch
            {
            }
            return val;
        }

        public static long GetLong(string key, long otherwise = 0)
        {
            var str = Get(key);
            var val = otherwise;
            try
            {
                val = long.Parse(str);
            }
            catch
            {
            }
            return val;
        }
    }
}

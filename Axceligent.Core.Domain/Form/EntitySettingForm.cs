﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Axceligent.Core.Domain.Form
{
    /// <summary>
    /// EntitySetting Form
    /// </summary>
    public class EntitySettingForm : FormBase<long>
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string EntityType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public long EntityRef { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string SKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string SVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string ParamType { get; set; }

    }
}

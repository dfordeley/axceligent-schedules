﻿using Axceligent.Core.Data.DB;
using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Axceligent.Core.Data.Repository
{
    public class BaseRepository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : EntityBase<TKey>
    {
        public AxDbContext _context;
        public BaseRepository(AxDbContext context)
        {
            _context = context;
        }

        public virtual TEntity Update(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.UtcNow;
                _context.Entry(obj).State = EntityState.Modified;
                _context.SaveChanges();
                return obj;
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return obj;
            }
        }


        public virtual int Delete(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.UtcNow;
                obj.RecordStatus = RecordStatus.Deleted;
                _context.Entry(obj).State = EntityState.Modified;
                return _context.SaveChanges();
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return 0;
            }
        }

        public virtual TEntity Insert(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.UtcNow;
                obj.CreatedAt = DateTime.UtcNow;
                _context.Set<TEntity>().Add(obj);
                _context.SaveChanges();
                return obj;

            }
           
            catch (Exception e)
            {
                //Log.Error(e);
                return obj;
            }
        }

        public virtual int DeleteFinally(TEntity obj)
        {
            try
            {
                _context.Set<TEntity>().Remove(obj);
                return _context.SaveChanges();
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return 0;
            }
        }

        public virtual int Count()
        {
            try
            {
                return Query().Count();

            }
            catch (Exception e)
            {
                //Log.Error(e);
                return 0;
            }
        }

        public virtual IQueryable<TEntity> Query(string includeProperties = "")
        {
            try
            {
                return All(includeProperties)
                    .Where(x => x.RecordStatus != RecordStatus.Deleted && x.RecordStatus != RecordStatus.Archive);
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return null;
            }
        }

        public virtual IQueryable<TEntity> All(string includeProperties = "")
        {
            try
            {
                IQueryable<TEntity> query = _context.Set<TEntity>();
                if (!String.IsNullOrEmpty(includeProperties))
                {
                    foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        query = query.Include(includeProperty);
                    }
                }
                return query;
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return null;
            }


        }

        public bool Exists(TKey id)
        {
            try
            {
                var i = _context.Set<TEntity>().Find(id);
                if (i != null)
                {
                    if (i.RecordStatus == RecordStatus.Deleted || i.RecordStatus == RecordStatus.Archive)
                    {
                        i = null;
                    }
                }
                return i != null;
            }

            catch (Exception e)
            {
               // Log.Error(e);
                return false;
            }
        }

        public TEntity Get(TKey id)
        {
            try
            {
                var i = _context.Set<TEntity>().Find(id);
                if (i != null)
                {
                    if (i.RecordStatus == RecordStatus.Deleted || i.RecordStatus == RecordStatus.Archive)
                    {
                        i = null;
                    }
                    else
                    {
                        //_context.Entry<TEntity>(i).Reload();
                    }
                }
                return i;
            }
            catch (Exception e)
            {
                //Log.Error(e);
                return null;
            }
        }

        public virtual TEntity DeleteUndo(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.Now;
                obj.RecordStatus = RecordStatus.Active;
                _context.Entry(obj).State = EntityState.Modified;
                _context.SaveChanges();
                return obj;
            }

            catch (Exception e)
            {
                //Log.Error(e);
                return obj;
            }
        }
    }
}

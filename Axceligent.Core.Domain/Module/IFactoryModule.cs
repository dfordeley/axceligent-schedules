﻿using Axceligent.Core.Domain.Factory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Module
{
    public class IFactoryModule
    {
        
        EntitySettingFactory EntitySettings { get; }
        EngineerFactory Engineers { get; }
        ShiftFactory Shifts { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AxceligentTest
{
    public class EngineerTest : UnitTest
    {
        [Fact]
        public void Engineer()
        {
            var engineerMModel = FactoryModule.Engineers.DefaultModel();
            var engineerForm= FactoryModule.Engineers.DefaultForm();
            var engineerEntity = FactoryModule.Engineers.DefaultEntity();
            var engineerListMModel = FactoryModule.Engineers.ListOfModel();
            var engineerListForm = FactoryModule.Engineers.ListOfForm();
            var engineeListEntity = FactoryModule.Engineers.ListOfEntity();

            Assert.True(engineerMModel != null);
            Assert.True(engineerForm != null);
            Assert.True(engineerEntity != null);
            Assert.True(engineerListMModel != null);
            Assert.True(engineerListForm != null);
            Assert.True(engineeListEntity != null);

            Assert.Empty(engineerListMModel);
            Assert.Empty(engineerListForm);
            Assert.Empty(engineeListEntity);
        }
    }
}

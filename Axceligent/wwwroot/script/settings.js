﻿$(document).ready(function () {

});
//Angular St
var EbipsApp = angular.module('EbipsApp', []);
function getNgScope() {
    var appElement = document.querySelector('[ng-app=EbipsApp]');
    return angular.element(appElement).scope();
};
var base = "http://localhost:54572";
EbipsApp.controller('EbipsRevCtrlr', function ($scope, $http) {
    $scope.processing = false;
    $scope.rowCollection = [{}];
    $scope.rev = {};
    $scope.formMode = 1;
    $scope.LocationId = 0;
    $scope.action = '';

    // display modal to add and edit Settings
    $scope.showSettingsModal = function () {
        $scope.formMode = 1;
        $scope.action = 'Add New Settings';
        $scope.rev = {};
        $("#addSettingsModal").modal("show");
    };
    //build datatable
    $scope.loadDataTable = function () {
        var lstRows = $scope.rowCollection;
        var hr = '';
        $("#listOfRecord").html("");
        hr = '<table id="datatable" class="table table-striped table-bordered table-actions-bar">';
        hr += '<thead><tr>';
        hr += '<th>S/N </th>';
        hr += '<th>Entity Type</th>';
        hr += '<th>Entity Ref</th>';
        hr += '<th> Key</th>';
        hr += '<th>Val</th>';
        hr += '<th>ParamT ype</th>';
        hr += '<th>Action</th>';
        hr += '</tr></thead><tbody>';
        $.each(lstRows, function (i, rec) {
            hr +=
                "<tr><td>" + rec.SN +
                "</td><td>" + rec.entityType +
                "</td><td>" + rec.entityRef +
                "</td><td>" + rec.sKey +
                "</td><td>" + rec.sVal +
                "</td><td>" + rec.paramType + "</td>";
            hr += "<td><a href='#' class='btn btn-primary' onclick='EditLocation(" + rec.id + ")'  title='Edit'></a>";
            hr += "<a href='#' class='btn btn-danger' onclick='DeleteLocation(" + rec.id + ")'</a></td>";
            hr += "</tr>";
        });
        hr += '</tbody>';

        hr += '</table>';
        $("#listOfRecord").html(hr);
    };
    $scope.loadDataTable();

    // call api to get  Settings
    $scope.getSettings = function () {
        $.ajax({
            type: "GET",
            url: base + "/api/Settings/Get",
            async: false
        })
            .success(function (data) {
                console.log(data);
                var resultData = [];
                var sn = 1;
                $.each(data, function (i, rec) {
                    rec.SN = sn;
                    resultData.push(rec);
                    sn++;
                });

                $scope.rowCollection = resultData;
                $scope.loadDataTable();

            })
            .error(function (data) {

                $scope.rowCollection = {};
                $scope.loadDataTable();
            });
    };
    $scope.getSettings();

    //Save Settings
    $scope.saveSettings = function () {
        // Adding a new Record
        if ($scope.formMode == 1) {
            if ($scope.rev.entityType == "" || $scope.rev.entityType == undefined) {
                alert("Input entity Type")
                return;
            }

            if ($scope.rev.entityRef == "" || $scope.rev.entityRef == undefined) {
                alert("Please Input entity Ref");

                return;
            }
            if ($scope.rev.sKey == "" || $scope.rev.sKey == undefined) {
                alert("Please Input Key");

            }
            if ($scope.rev.sVal == "" || $scope.rev.sVal == undefined) {
                alert("Please Input Key");

            }
            if ($scope.rev.sKey == "" || $scope.rev.sKey == undefined) {
                alert("Please Input Value");

            }

            $scope.processing = true;
            $(".disabledCtrl").removeAttr("disabled");
            //$("#loader").show();

            $.ajax({
                type: "POST",
                url: base + "/api/Settings/Create",
                async: true,
                data: {
                    EntityType: $scope.rev.entityType,
                    EntityRef: $scope.rev.entityRef,
                    SKey: $scope.rev.sKey,
                    SVal: $scope.rev.sVal,
                    ParamType: $scope.rev.paramType

                },
                datatype: 'jsonp',
                success: function (data) {
                    if (data != null) {

                        alert("Successful");
                        $scope.getSettings();
                        $(".disabledCtrl").removeAttr("disabled");
                        //$("#loader").hide();

                        $scope.processing = false;
                        $("#addSettingsModal").modal("hide");
                        $scope.rev = {};
                        // location.reload()
                    }
                    else {

                        alert('An Error Occured');

                        $scope.processing = false;
                        $(".disabledCtrl").removeAttr("disabled");
                        //$("#loader").hide(); 

                    }

                },
                error: function (data) {
                    console.log(data);
                    alert('An Error Occured');

                    $scope.processing = false;
                    $(".disabledCtrl").removeAttr("disabled");
                    //$("#loader").hide(); 

                }
            });

        }
        //Editing Existing Record
        else if ($scope.formMode == 2) {
            if ($scope.rev.entityType == "" || $scope.rev.entityType == undefined) {
                alert("Input Entity Type")
                return;
            }

            if ($scope.rev.entityRef < 1) {
                alert("Please Input Entity Ref");

                return;
            }
            if ($scope.rev.sKey == "" || $scope.rev.sKey == undefined) {
                alert("Please Input Key");

            }
            if ($scope.rev.sVal == "" || $scope.rev.sVal == undefined) {
                alert("Please Input Value");

            }

            $scope.processing = true;
            $(".disabledCtrl").removeAttr("disabled");
            //$("#loader").show();

            $.ajax({
                type: "POST",
                url: base + "/api/Settings/Update",
                async: true,
                data: {
                    Id: $scope.SettingsId,
                    EntityType: $scope.rev.entityType,
                    EntityRef: $scope.rev.entityRef,
                    SKey: $scope.rev.sKey,
                    SVal: $scope.rev.sVal,
                    ParamType: $scope.rev.paramType

                },
                datatype: 'jsonp',
                success: function (data) {
                    if (data != null) {

                        alert("Successful");
                        $scope.getSettings();
                        $(".disabledCtrl").removeAttr("disabled");
                        //$("#loader").hide();

                        $scope.processing = false;
                        $("#addSettingsModal").modal("hide");
                        $scope.rev = {};
                    }
                    else {

                        alert('An Error Occured');

                        $scope.processing = false;
                        $(".disabledCtrl").removeAttr("disabled");
                        //$("#loader").hide(); 

                    }

                },
                error: function (data) {
                    console.log(data);
                    alert('An Error Occured');

                    $scope.processing = false;
                    $(".disabledCtrl").removeAttr("disabled");
                    //$("#loader").hide(); 

                }
            });

        }
    };
});
function DeleteLocation(Id) {
    var $scope = getNgScope();

    var rowCollection = $scope.rowCollection;

    var row = [];
    for (var i = 0; i < rowCollection.length; i++) {
        if (rowCollection[i].Id == Id) {
            row = rowCollection[i];
            break;
        };
    }
    $scope.$apply(function () {
        $scope.rev = row;

    });


    confirm("are you sure you want to delete?");
    if (confirm) {
        $.ajax({
            type: "POST",
            url: base + "/api/Settings/Delete",
            async: true,
            data: {
                Id: Id,
            },
            datatype: 'jsonp',
            success: function (data) {


                alert("Successful");
                $scope.getSettings();
                $(".disabledCtrl").removeAttr("disabled");
                $scope.processing = false;
                $scope.rev = {};
            },
            error: function (data) {


            }
        });
    }




}


function EditLocation(Id) {

    var $scope = getNgScope();
    $scope.rev = {};
    $scope.action = 'Edit Setting';
    // $scope = $scope.$$childHead;
    //Edit Mode
    $scope.formMode = 2;
    var rowCollection = $scope.rowCollection;
    var row = [];
    for (var i = 0; i < rowCollection.length; i++) {
        if (rowCollection[i].id == Id) {
            row = rowCollection[i];
            break;
        };
    }
    $scope.$apply(function () {
        $scope.rev = row;

        console.log(row);
        $scope.SettingsId = row.id;
    });

    $("#addSettingsModal").modal("show");
}
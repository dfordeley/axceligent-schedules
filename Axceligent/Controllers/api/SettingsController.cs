﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axceligent.Core.Common;
using Axceligent.Core.Data.DB;
using Axceligent.Core.Domain.Form;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Axceligent.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Settings")]
    public class SettingsController : BaseApiController
    {
        private readonly AxDbContext _context;
        public SettingsController(AxDbContext context) : base(context)
        {
            _context = context;
        }
        /// <summary>
        /// Get settings
        /// </summary>
        /// <returns></returns>
        [Route("Get")]
        public IActionResult Get()
        {
            try
            {
                var data = Service.EntitySettings.Search();
                return Ok(data);
            }
            catch (Exception ex)
            {

                FileLog.Error(ex);
                return BadRequest();
            }

        }
        /// <summary>
        /// Create new settings        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        public IActionResult Create(EntitySettingForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelError(ModelState));
                }

                var entity = FactoryModule.EntitySettings.CreateEntity(form);
                entity = Service.EntitySettings.Insert(entity);
                if (entity.Id > 0)
                    return Ok(entity);

            }
            catch (Exception ex)
            {

                FileLog.Error(ex);
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Update settings
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        public IActionResult Update(EntitySettingForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelError(ModelState));
                }

                var entity = Service.EntitySettings.Get(form.Id);
                if(entity == null)
                {
                    return BadRequest("Invalid data");
                }
                var updatedEntity = FactoryModule.EntitySettings.CreateEntity(form);
                updatedEntity = Service.EntitySettings.Update(updatedEntity);
                if (updatedEntity.Id > 0)
                    return Ok(entity);

            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Delete settings
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [Route("Delete")] 
        public IActionResult Delete(long Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("");
                }

                var entity = Service.EntitySettings.Get(Id);
                if (entity == null)
                {
                    return BadRequest("Invalid data");
                }
                var deleteEntity = Service.EntitySettings.Delete(entity);
                if (deleteEntity > 0)
                    return Ok(deleteEntity);

            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest();
            }
            return Ok();
        }
    }
}
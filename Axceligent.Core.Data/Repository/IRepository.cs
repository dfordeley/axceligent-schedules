﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axceligent.Core.Data.Repository
{
    public interface IRepository<TEntity, TKey>
    {
        TEntity Get(TKey id);
        TEntity Update(TEntity obj);
        TEntity Insert(TEntity obj);
        int Delete(TEntity obj);
        TEntity DeleteUndo(TEntity obj);
        int DeleteFinally(TEntity obj);
        bool Exists(TKey id);
        int Count();
        IQueryable<TEntity> Query(string includeProperties = "");
        IQueryable<TEntity> All(string includeProperties = "");
    }
}

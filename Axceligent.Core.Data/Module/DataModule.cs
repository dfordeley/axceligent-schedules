﻿using Axceligent.Core.Data.DB;
using Axceligent.Core.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Data.Module
{
    public class DataModule: IDataModule
    {
        private AxDbContext _context;
        public DataModule(AxDbContext context)
        {
            _context = context;
        }
        private EntitySettingRepository _entitysettings;
        private ShiftRepository _shift;
        public EntitySettingRepository EntitySettings { get { if (_entitysettings == null) { _entitysettings = new EntitySettingRepository(_context); } return _entitysettings; } }
        public ShiftRepository Shifts { get { if (_shift == null) { _shift = new ShiftRepository(_context); } return _shift; } }
    }
}

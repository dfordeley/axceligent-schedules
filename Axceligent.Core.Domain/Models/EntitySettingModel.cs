﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Models
{
    /// <summary>
    /// EntitySetting View Model
    /// </summary>
    public class EntitySettingModel : ModelBase<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string EntityType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long EntityRef { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ParamType { get; set; }

    }
}

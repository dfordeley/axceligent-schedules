﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axceligent.Core.Data.DB;
using Axceligent.Core.Data.Module;
using Axceligent.Core.Domain.Module;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Axceligent.Controllers.api
{

    public class BaseApiController : Controller
    {
        private AxDbContext _context;

        private DataModule _service;
        public BaseApiController(AxDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// DB Service Wrapper for repositories
        /// </summary>
        public DataModule Service
        {
            get
            {
                if (_service == null)
                {
                    //if (_context == null)
                    //{
                    //    _context = new AxDbContext();
                    //}
                    
                }
              _service = new DataModule(_context);
              return _service;
            }
        }
        private FactoryModule _factory;

        /// <summary>
        /// 
        /// </summary>
        public FactoryModule FactoryModule
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new FactoryModule();
                }
                return _factory;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public string ModelError(ModelStateDictionary modelState)
        {
            string error = "";
            foreach (var state in modelState.Values)
            {
                foreach (var msg in state.Errors)
                {
                    error += msg.ErrorMessage + "<br />";
                }
            }
            return error;
        }

    }
}
﻿using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Form;
using Axceligent.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Factory
{
    /// <summary>
    /// EntitySetting Factory
    /// </summary>
    public class EntitySettingFactory : BaseFactory<EntitySetting, EntitySettingModel, EntitySettingForm, long>
    {

    }
}

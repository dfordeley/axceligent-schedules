using Axceligent.Core.Domain.Module;
using System;
using Xunit;

namespace AxceligentTest
{
    public class UnitTest
    {
        private FactoryModule _factory;

        public FactoryModule FactoryModule
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new FactoryModule();
                }
                return _factory;
            }
        }

    }
}

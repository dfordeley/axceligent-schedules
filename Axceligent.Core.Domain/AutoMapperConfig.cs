﻿using AutoMapper;
using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Form;
using Axceligent.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain
{
    public static class AutoMapperConfig
    {
        /// <summary>
        /// Registers the mappings.
        /// </summary>
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {

                

                cfg.CreateMap<EntitySetting, EntitySettingModel>().ReverseMap();
                cfg.CreateMap<EntitySetting, EntitySettingForm>().ReverseMap();
                cfg.CreateMap<EntitySettingModel, EntitySettingForm>().ReverseMap();

                
            });
        }
    }
}

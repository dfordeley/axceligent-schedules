﻿using Axceligent.Core.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Data.Module
{
    public class IDataModule
    {
        EntitySettingRepository EntitySettings { get; }
        ShiftRepository Shifts { get; }
    }
}

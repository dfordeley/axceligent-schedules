﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Axceligent.Core.Domain.Entity
{
    /// <summary>
    /// EntitySetting Class
    /// </summary>
    public class EntitySetting : EntityBase<long>
    {
        /// <summary>
        /// 
        /// </summary>
        
        [MaxLength(64)]
        public string EntityType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long EntityRef { get; set; }
        /// <summary>
        /// 
        /// </summary>
        
        [MaxLength(64)]
        public string SKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
       
        [MaxLength(128)]
        public string SVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        
        [MaxLength(64)]
        public string ParamType { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Models
{

    public class SelectMenuModel<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
    }


    public class DataAggregateModel<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
        public long Records { get; set; }
    }
}

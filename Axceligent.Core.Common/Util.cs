﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Security.Cryptography;
namespace Axceligent.Core.Common
{
    public static class Util
    {


        public static object ShapeList<TSource>(this IList<TSource> obj, string fields)
        {
            List<string> lstOfFields = new List<string>();
            if (string.IsNullOrEmpty(fields))
            {
                return obj;
            }
            lstOfFields = fields.Split(',').ToList();
            List<string> lstOfFieldsToWorkWith = new List<string>(lstOfFields);

            List<System.Dynamic.ExpandoObject> lsobjectToReturn = new List<System.Dynamic.ExpandoObject>();
            if (!lstOfFieldsToWorkWith.Any())
            {
                return obj;
            }
            else
            {



                foreach (var kj in obj)
                {

                    System.Dynamic.ExpandoObject objectToReturn = new System.Dynamic.ExpandoObject();

                    foreach (var field in lstOfFieldsToWorkWith)
                    {
                        try
                        {
                            var fieldValue = kj.GetType()
                            .GetProperty(field.Trim(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                            .GetValue(kj, null);
                            ((IDictionary<String, Object>)objectToReturn).Add(field.Trim(), fieldValue);
                        }
                        catch
                        {
                        }

                    }

                    lsobjectToReturn.Add(objectToReturn);
                }
            }
            return lsobjectToReturn;
        }

        public static DataTable ToDataTable<TSource>(this IList<TSource> data)
        {
            DataTable dataTable = new DataTable(typeof(TSource).Name);
            PropertyInfo[] props = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (TSource item in data)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        
        public static string ToText(object v)
        {
            return v.ToString();
        }

        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }


       

        public static decimal ToDecimal(string amount)
        {
            try
            {
                return Convert.ToDecimal(amount);
            }
            catch
            {
                return 0m;
            }
        }

        public static byte[] ReadBytesOfFile(string fle, bool delete = false)
        {
            byte[] bytes = new byte[1];
            try
            {
                using (FileStream fsSource = new FileStream(fle, FileMode.Open, FileAccess.Read))
                {
                    bytes = new byte[fsSource.Length];
                    int numBytesToRead = (int)fsSource.Length;
                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        int n2 = fsSource.Read(bytes, numBytesRead, numBytesToRead);
                        if (n2 == 0)
                        {
                            fsSource.Close();
                            fsSource.Dispose();
                            break;
                        }
                        numBytesRead += n2;
                        numBytesToRead -= n2;
                    }
                }
                if (delete)
                {
                    FileInfo fi = new FileInfo(fle);
                    fi.Delete();
                }
            }
            catch
            {
            }
            return bytes;
        }

     

        public static string GetString(object k)
        {
            if (k == null) return "";
            return k.ToString();
        }

        public static string Checkable(this bool chk)
        {
            if (chk)
            {
                return "checked";
            }
            return "";
        }

        public static string ConvertToRegex(string bit)
        {
            //[D:11:11]
            //[R:100:50000]
            //[L:A:Z]

            char[] trima = { '[', ']' };
            var txt = bit.Trim(trima);
            char[] sep = { ':' };

            string[] bits = txt.ToUpper().Split(sep);
            if (bits.Length != 3)
            {
                throw new Exception("Regex build parts is not 3 characters");
            }
            var resp = "";
            var n1 = 0;
            var n2 = 0;
            switch (bits[0])
            {
                case "D":
                    n1 = ToInt32(bits[1]);
                    n2 = ToInt32(bits[2]);
                    resp = @"\d{" + n1 + "," + n2 + "}";
                    break;
                case "R":
                    break;
                case "A":
                    resp = @"\w{" + n1 + "," + n2 + "}";
                    break;
            }
            return resp;
        }

        public static DateTime DefaultDate()
        {
            return new DateTime(1970, 1, 1);
        }

        public static DateTime ToDateTime(string datetime)
        {
            var dtm = DateTime.Now;
            if (!DateTime.TryParse(datetime, out dtm))
            {
                dtm = DefaultDate();
            }
            return dtm;
        }

        public static DateTime CurrentDateTime()
        {
            return DateTime.UtcNow;
        }

        public static string DecimalToArbitrarySystem(long decimalNumber, int radix)
        {
            const int BitsInLong = 64;
            const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (radix < 2 || radix > Digits.Length)
                throw new ArgumentException("The radix must be >= 2 and <= " + Digits.Length.ToString());

            if (decimalNumber == 0)
                return "0";

            int index = BitsInLong - 1;
            long currentNumber = Math.Abs(decimalNumber);
            char[] charArray = new char[BitsInLong];

            while (currentNumber != 0)
            {
                int remainder = (int)(currentNumber % radix);
                charArray[index--] = Digits[remainder];
                currentNumber = currentNumber / radix;
            }

            string result = new string(charArray, index + 1, BitsInLong - index - 1);
            if (decimalNumber < 0)
            {
                result = "-" + result;
            }

            return result;
        }

        public static T DeserializeJSON<T>(string objectData)
        {
            return JsonConvert.DeserializeObject<T>(objectData);
        }

        public static T DeserializeXML<T>(string objectData)
        {
            var serializer = new XmlSerializer(typeof(T));
            object result;
            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }
            return (T)result;

        }
        public static string[] Explode(string txt, string delim = ",", int opt = 1)
        {
            string[] arr = null;
            txt = txt.Trim();
            if (txt == "") return arr;
            if (delim.Length != 1) return arr;
            char[] sep = delim.ToCharArray();
            try
            {
                if (opt == 1)
                    arr = txt.Split(sep, StringSplitOptions.None);
                else
                    arr = txt.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            }
            catch
            {
                //Log.Write("StringSplit: " + txt + " " + delim + " " + ex.Message);
            }
            return arr;
        }

        public static string GetDifferenceInData(object newObject, object oldObject, object id)
        {
            string resp = "{'id':" + id.ToString();
            PropertyInfo[] properties = newObject.GetType().GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                string oldpp = GetPropValue(oldObject, pi.Name);
                string newpp = GetPropValue(newObject, pi.Name);

                if (newpp != oldpp)
                {
                    resp += ", '" + pi.Name + "': [{'_old':'" + oldpp + "'},{'_new':'" + newpp + "'}]";
                }

            }
            resp += "}";
            return resp;
        }

        public static string CleanPhone(string phone)
        {
            phone = phone.Replace("+", "");
            return phone.Trim();
        }

        public static string GetPropValue(object src, string propName)
        {
            string resp = "";
            try
            {
                resp = src.GetType().GetProperty(propName).GetValue(src, null).ToString();
            }
            catch
            {
                resp = "";

            }
            return resp;
        }

        public static bool IsEmail(string emailAddress)
        {
            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                 + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                 + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                 + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                 + @"[a-zA-Z]{2,}))$";
            Regex reStrict = new Regex(patternStrict);
            bool isStrictMatch = reStrict.IsMatch(emailAddress);
            return isStrictMatch;
        }

        public static string JavaScriptSafeText(string txt)
        {
            return txt;
        }

        public static bool MatchPattern(string pattern, string text)
        {
            Regex reStrict = new Regex(pattern);
            bool isStrictMatch = reStrict.IsMatch(text);
            return isStrictMatch;
        }

        public static string ObjectToString(object obj)
        {
            string resp = "";
            PropertyInfo[] properties = obj.GetType().GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                string oldpp = GetPropValue(obj, pi.Name);
                resp += pi.Name + ": " + oldpp + Environment.NewLine;
            }
            return resp;
        }

        public static string SerializeJSON(object objectInstance)
        {
            return JsonConvert.SerializeObject(objectInstance);
        }

        public static string SerializeXML(object objectInstance)
        {
            string txt = "";
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(objectInstance.GetType());
            var settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = false;
            settings.Encoding = new UTF8Encoding(false);
            settings.ConformanceLevel = ConformanceLevel.Document;
            var memoryStream = new MemoryStream();
            using (var writer = XmlWriter.Create(memoryStream, settings))
            {
                serializer.Serialize(writer, objectInstance, emptyNamepsaces);
                txt = Encoding.UTF8.GetString(memoryStream.ToArray());
            }
            return txt;
        }

        public static string TimeStampCode(string prefix = "")
        {
            Thread.Sleep(1);
            string stamp = DateTime.Now.ToString("yyMMddHHmmssffffff");
            long num = long.Parse(stamp);
            return prefix + DecimalToArbitrarySystem(num, 36);
        }

        public static bool Unzip(string zipfle, string destFolder)
        {
            try
            {
                System.IO.Compression.ZipFile.ExtractToDirectory(zipfle, destFolder);
                return true;
            }
            catch
            {
                //Log.Error(ex);
                return false;
            }
        }

        public static bool FileExists(string destFile0)
        {
            return File.Exists(destFile0);
        }

        public static void FileCopy(string destFile0, string destFile1)
        {
            File.Copy(destFile0, destFile1);
        }

        public static void FileDelete(string destFile0)
        {

            File.Delete(destFile0);
        }

        public static string TimeStampCode(DateTime dtm, string prefix = "")
        {
            Thread.Sleep(1);
            string stamp = dtm.ToString("yyMMddHHmmssffffff");
            long num = long.Parse(stamp);
            return prefix + DecimalToArbitrarySystem(num, 36);
        }

        public static double ToDouble(object v)
        {
            double resp = 0;
            try
            {
                resp = Convert.ToDouble(v);
            }
            catch { }
            return resp;
        }
        public static int ToInt32(object p)
        {
            int resp = 0;
            try
            {
                resp = Convert.ToInt32(p);
            }
            catch { }
            return resp;
        }

        public static long ToInt64(object p)
        {
            long resp = 0;
            try
            {
                resp = Convert.ToInt64(p);
            }
            catch { }
            return resp;
        }
        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }

        public static string Sha256(string text)
        {
            byte[] value = Encoding.UTF8.GetBytes(text);
            var data = new SHA256Managed().ComputeHash(value);
            return ByteToString(data);
        }

        public static string ByteToString(byte[] buff)
        {
            string getbinary = "";
            for (int i = 0; i <= buff.Length - 1; i++)
            {
                getbinary += buff[i].ToString("X2");
            }
            return getbinary;
        }

    }
}

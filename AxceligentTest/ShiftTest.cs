﻿
using Axceligent.Core.Domain.Module;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AxceligentTest
{
    public class ShiftTest : UnitTest
    {
        [Fact]
        public void ShiftModelTest()
        {
            var defaultModel = FactoryModule.Shifts.DefaultModel();
            Assert.NotNull(defaultModel);
            
            var defaultList = FactoryModule.Shifts.ListOfEntity();
            Assert.Empty(defaultList);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axceligent.Core.Data.DB;
using Axceligent.Core.Data.Module;
using Axceligent.Core.Domain.Module;
using Microsoft.AspNetCore.Mvc;

namespace Axceligent.Controllers
{
    public class BaseController : Controller
    {
        private AxDbContext _context;

        private DataModule _service;
        public BaseController(AxDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// DB Service Wrapper for repositories
        /// </summary>
        public DataModule Service
        {
            get
            {
                _service = new DataModule(_context);
                return _service;
            }
        }
        private FactoryModule _factory;

        /// <summary>
        /// 
        /// </summary>
        public FactoryModule FactoryModule
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new FactoryModule();
                }
                return _factory;
            }
        }

    }
}
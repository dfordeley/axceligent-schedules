﻿using Axceligent.Core.Domain.Module;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AxceligentTest
{
    public class SettingsTest: UnitTest
    {
        [Fact]
        public void Settings()
        {
            var entity = FactoryModule.EntitySettings.DefaultEntity();
            Assert.True(entity != null);

            var settingsMModel = FactoryModule.EntitySettings.DefaultModel();
            var settingsForm = FactoryModule.EntitySettings.DefaultForm();
            var settingsEntity = FactoryModule.EntitySettings.DefaultEntity();
            var settingsListMModel = FactoryModule.EntitySettings.ListOfModel();
            var settingsListForm = FactoryModule.EntitySettings.ListOfForm();
            var settingsListEntity = FactoryModule.EntitySettings.ListOfEntity();

            Assert.True(settingsMModel != null);
            Assert.True(settingsForm != null);
            Assert.True(settingsEntity != null);
            Assert.True(settingsListMModel != null);
            Assert.True(settingsListForm != null);
            Assert.True(settingsListEntity != null);
            Assert.Empty(settingsListMModel);
            Assert.Empty(settingsListForm);
            Assert.Empty(settingsListEntity);


        }
    }
}

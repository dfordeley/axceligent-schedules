﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Axceligent.Core.Common;
using Axceligent.Core.Data.DB;
using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Form;
using Axceligent.Core.Domain.Helper;
using Axceligent.Core.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Axceligent.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Schedules")]
    public class SchedulesController : BaseApiController
    {
        private readonly AxDbContext _context;
        public SchedulesController(AxDbContext context) : base(context)
        {
            _context = context;
        }

        [Route("Generate")]
        public IActionResult Generate()
        {


            try
            {

                var settings = Service.EntitySettings.Search(entityType: "RULES").ToList();
                var settingHelper = new EntitySettingHelper(settings);
                var maxDaily = settingHelper.GetInt("MAXDAILYSHIFT");
                var noOfShift = settingHelper.GetInt("NUMBEROFSHIFTS");
                var minShift = settingHelper.GetInt("MINSHIFTPERDURATION");
                var consecHol = settingHelper.GetInt("CONSECDAYSHOL");
                var numofEngineers = settingHelper.GetInt("NOFENGINEERS");
                var scheduleDuration = settingHelper.GetInt("SCHEDULEDURATION");
                var consecAfternoons = settingHelper.GetInt("CONSECNOON");

                Random random = new Random();
                var scheduleList = new List<ShiftModel>();
                var engineerList = new List<EngineerModel>();


                //Generate default vaules for all the enigneers -- Which is N engineers  based on what was set
                for (int i = 0; i < numofEngineers; i++)
                {
                    //Generate default vaules for all the enigneers
                    //Lets engineers ID start from 1
                    var newEngineer = FactoryModule.Engineers.DefaultModel();
                    newEngineer.Id = i + 1;
                    engineerList.Add(newEngineer);
                }

                // Now, Let's generate the schedules -- the number of shifts is number of days multiplied by shifts per days
                var totalShifts = noOfShift * scheduleDuration;

                var noOfShiftsOff = consecHol * noOfShift;
                for (int j = 0; j < totalShifts; j++)
                {
                    // For each shifts, randomly assign and engineer, using the rules defined.
                    int randomValue = 1;
                    //To make sure each engineer gets the required minimum shifts per duration, randomise engineers with less than min req schedules
                    if (engineerList.Where(x => x.ScheduleCount < minShift).Any())
                    {
                        var UnscheduledEng = engineerList.Where(x => x.ScheduleCount < 2).Select(x => x.Id);
                        randomValue = UnscheduledEng.RandomHelper();
                    }
                    else
                        randomValue = random.Next(1, numofEngineers + 1);

                    var shiftDetails = new ShiftModel();
                    //Get Generated Engineer 
                    var generatedEngineer = engineerList.Where(x => x.Id == randomValue).FirstOrDefault();

                    //Check Engineer against the rules
                    var verdict = EngineerValidator(generatedEngineer, j + 1, noOfShiftsOff);

                    int RandomCounter = 0;
                    int WhileCounter = 0;
                    while (!verdict)
                    {
                        if (WhileCounter > 500)
                        {
                            break;

                        }
                        else if (engineerList.Where(x => x.ScheduleCount < minShift).Any() && RandomCounter < 100)
                        {

                            // Try to suggest an Engineer that has been scheduled less twice
                            var UnscheduledEng = engineerList.Where(x => x.ScheduleCount < 2);
                            generatedEngineer = UnscheduledEng.RandomHelper();
                            verdict = EngineerValidator(generatedEngineer, j + 1, noOfShiftsOff);

                            //I had to introduce a random counter because, there are cases when the rules cannot allow the remaining engineers with schedules less than the minimun
                            RandomCounter++;
                        }
                        else
                        {

                            randomValue = random.Next(1, numofEngineers + 1);
                            generatedEngineer = engineerList.Where(x => x.Id == randomValue).FirstOrDefault();
                            verdict = EngineerValidator(generatedEngineer, j + 1, noOfShiftsOff);

                        }

                        WhileCounter++;
                    }

                    if (WhileCounter > 500)
                    {
                        return BadRequest("Cannot generate accurate Schedule right now, Please try again!");

                    }

                    //Update engineer status, based on schedule

                    ////Check if it's consecutive Days
                    if (engineerList.Where(x => x.Id == generatedEngineer.Id).FirstOrDefault().LastScheduleId == j - 1 || engineerList.Where(x => x.Id == generatedEngineer.Id).FirstOrDefault().LastScheduleId == j)
                        generatedEngineer.NumberOfHoliday = consecHol;


                    generatedEngineer.LastScheduleId = j + 1;
                    generatedEngineer.ScheduleCount++;
                    if ((j + 1) % 2 == 1)
                        generatedEngineer.LastState = Core.Domain.Enums.States.MorningShift;
                    else
                        generatedEngineer.LastState = Core.Domain.Enums.States.AfternoonShift;

                    //Add shift details and add to list
                    shiftDetails.EngineerId = generatedEngineer.Id;
                    shiftDetails.ScheduleId = j + 1;

                    scheduleList.Add(shiftDetails);

                }
                FileLog.Info(Util.SerializeJSON(scheduleList));
                //Try to delete existing schedules if any exist

                var oldSchedules = Service.Shifts.Search().ToList();
                if(oldSchedules.Any())
                {
                    foreach(var items in oldSchedules)
                    {
                        Service.Shifts.Delete(items);
                    }
                }


                //Save Each Schedule
                foreach(var item in scheduleList)
                {
                    var shiftEntity = FactoryModule.Shifts.DefaultEntity();
                    shiftEntity.EngineerId = item.EngineerId;
                    shiftEntity.ScheduleId = item.ScheduleId;
                    Service.Shifts.Insert(shiftEntity);
                }

                return Ok(scheduleList);

            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                throw;
            }


        }
        [Route("Get")]
        public IActionResult Get()
        {
            try
            {
                var data = Service.Shifts.Search();
                var model =  data.Select(FactoryModule.Shifts.CreateModel).ToList();
                return Ok(model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private bool EngineerValidator(EngineerModel model, int newSheduleId, int noOfShiftsOff)
        {
            // check Consective Morning shifts
            if (model.NumberOfHoliday > 0 && model.LastScheduleId - noOfShiftsOff > 0)
            {
                return false;
            }

            //check consecutive shifts in a day
            if (model.LastState == Core.Domain.Enums.States.MorningShift && model.LastScheduleId == newSheduleId - 1)
            {
                return false;
            }

            //check consective afternoon shifts
            //afternoon shifts are alwyas even numbers, check if value is an even number
            if ((newSheduleId % 2) == 0 && model.LastState == Core.Domain.Enums.States.AfternoonShift)
            {
                return false;
            }
            return true;
        }
    }
}
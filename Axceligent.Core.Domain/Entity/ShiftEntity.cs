﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Entity
{
    public class ShiftEntity: EntityBase<int>
    {
        public int EngineerId { get; set; }
        public int ScheduleId { get; set; }
    }
}

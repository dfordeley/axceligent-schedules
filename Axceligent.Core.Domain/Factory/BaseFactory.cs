﻿using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Form;
using Axceligent.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Axceligent.Core.Domain.Factory
{
     public class BaseFactory<TEntity, TModel, TForm, TKey>
        where TEntity : EntityBase<TKey>
        where TModel : ModelBase<TKey>
        where TForm : FormBase<TKey>

    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public object Shape<T>(T obj, string fields)
        {
            List<string> lstOfFields = new List<string>();
            if (string.IsNullOrEmpty(fields))
            {
                return obj;
            }
            lstOfFields = fields.Split(',').ToList();
            List<string> lstOfFieldsToWorkWith = new List<string>(lstOfFields);
            if (!lstOfFieldsToWorkWith.Any())
            {
                return obj;
            }
            else
            {
                ExpandoObject objectToReturn = new ExpandoObject();
                foreach (var field in lstOfFieldsToWorkWith)
                {
                    try
                    {
                        var fieldValue = obj.GetType()
                        .GetProperty(field.Trim(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                        .GetValue(obj, null);
                        ((IDictionary<String, Object>)objectToReturn).Add(field.Trim(), fieldValue);
                    }
                    catch 
                    {
                        //Log.Error(ex);
                    }

                }
                return objectToReturn;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        //public virtual T Session<T>(string key)
        //{
        //    var obj = default(T);
        //    try
        //    {
        //        obj = (T)HttpContext.Current.Session[key];
        //    }
        //    catch
        //    {
        //    }
        //    return obj;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="src"></param>
        /// <param name="obj"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public T1 Patch<T1, T2>(T1 src, T2 obj, string fields)
        {
            List<string> lstOfFields = new List<string>();
            if (string.IsNullOrEmpty(fields))
            {
                return src;
            }
            lstOfFields = fields.Split(',').ToList();
            List<string> lstOfFieldsToWorkWith = new List<string>(lstOfFields);
            if (!lstOfFieldsToWorkWith.Any())
            {
                return src;
            }
            else
            {
                foreach (var field in lstOfFieldsToWorkWith)
                {

                    var fieldProp = obj.GetType()
                        .GetProperty(field.Trim(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                    var fieldValue = fieldProp.GetValue(obj, null);
                    var property = src.GetType()
                        .GetProperty(field.Trim(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                    Type t = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;

                    object safeValue = (fieldValue == null) ? null : Convert.ChangeType(fieldValue, t);
                    property.SetValue(src, fieldValue, null);




                }
                return src;
            }

        }


        /// <summary>
        /// Create an Entity Type from a Model Type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TEntity CreateEntity(TModel obj)
        {
            return DataMapper.Map<TEntity, TModel>(obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TEntity CreateEntity(TForm obj)
        {
            return DataMapper.Map<TEntity, TForm>(obj);
        }
        /// <summary>
        /// Create a Form Type from a Model Type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TForm CreateFrom(TModel obj)
        {
            return DataMapper.Map<TForm, TModel>(obj);
        }
        /// <summary>
        /// Create a Form Type from Entity Type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TForm CreateForm(TEntity obj)
        {
            return DataMapper.Map<TForm, TEntity>(obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TModel CreateModel(TEntity obj)
        {
            return DataMapper.Map<TModel, TEntity>(obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TModel CreateModel(TForm obj)
        {
            return DataMapper.Map<TModel, TForm>(obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual TModel DefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TModel> ListOfModel()
        {
            return new List<TModel>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual TEntity DefaultEntity()
        {
            return (TEntity)Activator.CreateInstance(typeof(TEntity));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> ListOfEntity()
        {
            return new List<TEntity>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual TForm DefaultForm()
        {
            return (TForm)Activator.CreateInstance(typeof(TForm));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TForm> ListOfForm()
        {
            return new List<TForm>();
        }
    }
}

﻿using Axceligent.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Form
{
    /// <summary>
    /// 
    /// </summary>
    public class FormBase<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public T Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public RecordStatus RecordStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Axceligent.Core.Data.DB;
using Axceligent.Core.Domain.Form;
using Axceligent.Core.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace Axceligent.Controllers
{
    public class EntitySettingsController : BaseController
    {
        private readonly AxDbContext _context;
        public EntitySettingsController(AxDbContext context) : base(context)
        {
            _context = context;
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}
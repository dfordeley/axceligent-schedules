﻿using Axceligent.Core.Data.DB;
using Axceligent.Core.Domain.Entity;
using Axceligent.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axceligent.Core.Data.Repository
{
    public class EntitySettingRepository : BaseRepository<EntitySetting, long>
    {
        public EntitySettingRepository(AxDbContext context) : base(context)
        {
        }

        public IQueryable<EntitySetting> Search(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(entityType))
            {
                table = table.Where(x => x.EntityType == entityType);
            }
            if (entityRef > 0)
            {
                table = table.Where(x => x.EntityRef == entityRef);
            }
            if (!string.IsNullOrEmpty(sKey))
            {
                table = table.Where(x => x.SKey == sKey);
            }
            if (!string.IsNullOrEmpty(sVal))
            {
                table = table.Where(x => x.SVal == sVal);
            }
            if (!string.IsNullOrEmpty(paramType))
            {
                table = table.Where(x => x.ParamType == paramType);
            }

            return table;
        }

        public bool ItemExists(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "", long Id = 0)
        {
            var check = Search(entityType, entityRef, sKey, sVal, paramType);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        public bool ItemExists(EntitySettingModel model, long Id = 0)
        {
            var check = Search(model.EntityType, model.EntityRef, model.SKey, model.SVal, model.ParamType);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}

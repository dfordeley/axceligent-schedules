﻿using Axceligent.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Axceligent.Core.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ModelBase<T>
    {
        /// <summary>
        /// 
        /// </summary> 
        public T Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public RecordStatus RecordStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Updated Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime UpdatedAt { get; set; }

        public virtual object LogMe()
        {
            return Id;
        }

        public string RecordStatusText
        {
            get
            {
                return RecordStatus.ToString();
            }
        }


        public string CreatedAtText
        {
            get
            {
                return CreatedAt.ToString("dd-MMM-yy HH:mm");
            }
        }


        public string UpdatedAtText
        {
            get
            {
                return UpdatedAt.ToString("dd-MMM-yy HH:mm");
            }
        }
    }

}

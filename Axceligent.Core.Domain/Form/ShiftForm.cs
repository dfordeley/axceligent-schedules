﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axceligent.Core.Domain.Form
{
    public class ShiftForm: FormBase<int>
    {
        public int EngineerId { get; set; }
        public int ScheduleId { get; set; }
    }
}

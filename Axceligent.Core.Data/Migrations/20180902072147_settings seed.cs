﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axceligent.Core.Data.Migrations
{
    public partial class settingsseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE() ,1,'RULES',0,'MAXDAILYSHIFT',1,'int');";
            migrationBuilder.Sql(sql: qry);

            qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE(),1,'RULES',0,'NUMBEROFSHIFTS',2,'int');";
            migrationBuilder.Sql(qry);

            qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE(),1,'RULES',0,'MINSHIFTPERDURATION',2,'int');";
            migrationBuilder.Sql(qry);

            qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE(),1,'RULES',0,'CONSECDAYSHOL',2,'int');";
            migrationBuilder.Sql(qry);

            qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE(),1,'RULES',0,'SCHEDULEDURATION',14,'int');";
            migrationBuilder.Sql(qry);

            qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE(),1,'RULES',0,'NOFENGINEERS',10,'int');";
            migrationBuilder.Sql(qry);

            qry = "insert into EntitySettings (CreatedAt,UpdatedAt,RecordStatus,EntityType,EntityRef,SKey,SVal,ParamType) values (GETDATE(),GETDATE(),1,'RULES',0,'CONSECNOON',0,'int');";
            migrationBuilder.Sql(qry);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
